import argparse
import os.path
import decisiontree.core.DecisionTreeClassifier as Dtc
import decisiontree.core.LearningSetBuilder as Lsb
import decisiontree.core.CrossValidation as Cv

my_path = os.path.abspath(os.path.dirname(__file__))

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-input', type=str, help='learningset path')
    parser.add_argument('-min_gain', type=float, help='minimum gain value')
    parser.add_argument('-spl_crt', type=str, help='split criteria {gini, entropy}')
    parser.add_argument('-dist', type=str, help='distance measure {l1, l2, dtw, cort, cort_dtw}')
    parser.add_argument('-ts_spl', type=int, help='Split method {1: Spherical, 2:Spherical/Hyperplane, 3:Hyperplane}')
    parser.add_argument('-method', type=str, help='distance measure {additive, average}')
    parser.add_argument('-w_type', type=str, help='weighting type {uniform, bw}')
    parser.add_argument('-w_fac', type=str, help='weighting factor {normal, square, sqrt}')
    parser.add_argument('-min_obs_node', type=int, help='minimum number of observations per node')
    parser.add_argument('-maj_class_per', type=float, help='majority class percentage per node')
    parser.add_argument('-max_depth', type=int, help='maximum depth of the tree')
    parser.add_argument('-window', type=float, help='window look-ahead for DTW, CorT and CorT_DTW')
    parser.add_argument('-unb_tree', type=int, help='Taking into account unbalanced '
                                                    'class distribution {0:False, 1:True}')
    parser.add_argument('-str_path', type=int, help='whether to store the path or not {0:False, 1:True}')
    parser.add_argument('-output', type=str, help='output path directory')
    parser.add_argument('-out', type=str, help='name of the output file')
    parser.add_argument('-testset', type=str, help='path to testset file')
    parser.add_argument('-cv', type=int, help='number of folds for cross-validation')
    parser.add_argument('-config', type=str, help='path to config file')

    args = parser.parse_args()

    if not args.input:
        raise IOError('No input learning set was given')

    if args.config:
        config_path = args.config
    else:
        config_path = my_path + '/' + 'TDT_builder.config'

    param_dict = dict()

    try:
        with open(config_path, 'r') as config_file:
            for line in config_file.readlines():
                split_line = line.replace('\n', '').split('=')
                split_line[0] = split_line[0].strip()
                if len(split_line) < 2:
                    param_dict[split_line[0]] = None
                else:
                    param_dict[split_line[0]] = split_line[1]
    except IOError as error:
        raise IOError('File ' + config_path + '  was not found')

    try:
        param_dict['min_gain'] = float(param_dict['min_gain'])
        param_dict['dist'] = [v.strip() for v in param_dict['dist'].split(',')]
        param_dict['spl_crt'] = param_dict['spl_crt']
        param_dict['ts_spl'] = int(param_dict['ts_spl'])
        param_dict['method'] = [v.strip() for v in param_dict['method'].split(',')]
        param_dict['w_type'] = [v.strip() for v in param_dict['w_type'].split(',')]
        param_dict['w_fac'] = [v.strip() for v in param_dict['w_fac'].split(',')]
        param_dict['min_obs_node'] = int(param_dict['min_obs_node'])
        param_dict['maj_class_per'] = float(param_dict['maj_class_per'])
        param_dict['max_depth'] = int(param_dict['max_depth'])
        if not param_dict['window']:
            param_dict['window'] = None
        param_dict['unb_tree'] = True if param_dict['str_path'] == '1' else False
        param_dict['str_path'] = True if param_dict['str_path'] == '1' else False
        if not param_dict['output']:
            param_dict['output'] = my_path + '/tree.json'

        if args.min_gain:
            param_dict['min_gain'] = float(args.min_gain)
        if args.dist:
            param_dict['dist'] = [v.strip() for v in args.dist.split(',')]
        if args.ts_spl:
            param_dict['ts_spl'] = int(args.ts_spl)
        if args.spl_crt:
            param_dict['spl_crt'] = args.spl_crt
        if args.method:
            param_dict['method'] = [v.strip() for v in args.method.split(',')]
        if args.w_type:
            param_dict['w_type'] = [v.strip() for v in args.w_type.split(',')]
        if args.w_fac:
            param_dict['w_fac'] = [v.strip() for v in args.w_fac.split(',')]
        if args.min_obs_node:
            param_dict['min_obs_node'] = int(args.min_obs_node)
        if args.maj_class_per:
            param_dict['maj_class_per'] = float(args.maj_class_per)
        if args.max_depth:
            param_dict['max_depth'] = int(args.max_depth)
        if args.window:
            param_dict['window'] = float(args.window)
        param_dict['str_path'] = True if args.str_path == 1 else False
        param_dict['unb_tree'] = True if args.str_path == 1 else False

        if args.output:
            param_dict['output'] = args.output
        param_dict['testset'] = args.testset if args.testset else None
        param_dict['cv'] = int(args.cv) if args.cv else None

    except AttributeError as error:
        raise AttributeError('Some arguments types are wrong')

    directory_path = param_dict['output'].split('/')
    directory_path = '/'.join(directory_path[0: len(directory_path) - 1])
    if directory_path and not os.path.exists(directory_path):
        raise IOError('Directory ' + directory_path + ' does not exist!')

    tree_classifier = Dtc.DecisionTreeClassifier(min_gain_percentage=param_dict['min_gain'],
                                                 split_criteria=param_dict['spl_crt'],
                                                 distance_measure=param_dict['dist'],
                                                 method=param_dict['method'],
                                                 weight_type=param_dict['w_type'],
                                                 weight_factor=param_dict['w_fac'],
                                                 store_path=param_dict['str_path'],
                                                 max_depth=param_dict['max_depth'],
                                                 ts_splitter=param_dict['ts_spl'],
                                                 min_observations_node=param_dict['min_obs_node'],
                                                 majority_class_percentage=param_dict['maj_class_per'],
                                                 window=param_dict['window'],
                                                 unbalanced_tree=param_dict['unb_tree'])
    ls_builder = Lsb.LearningSetBuilder()
    learningset = None
    try:
        learningset = ls_builder.build(ls_format='one_line',
                                       input_file=args.input,
                                       ls_name='learningset', obs_prefix='obs_train_', ts_prefix='TS_train_')
    except IOError as error:
        raise IOError('File ' + learningset + ' does not exist')

    tree = None
    if param_dict['testset'] is not None:
        testset = None
        try:
            testset = ls_builder.build(ls_format='one_line',
                                       input_file=param_dict['testset'],
                                       ls_name='testset', obs_prefix='obs_test_', ts_prefix='TS_test_')
        except IOError as error:
            raise IOError('File ' + testset + ' does not exist')

        try:
            print('Running tdt_train on ' + args.input + ' with testset ' + param_dict['testset'] + ':')
            tree = tree_classifier.fit(learningset)
        except RuntimeError as error:
            raise RuntimeError('Error occurred while generating the tree, please contact support')

        try:
            tree.predict(testset)
        except RuntimeError as error:
            raise RuntimeError('Error Occurred while testing the tree, please contact support')

    elif param_dict['cv']:
        print('Running tdt_train on ' + args.input + ' with cross-validation k=' + param_dict['cv'] + ':')
        tree = Cv.train_resampling(classifier=tree_classifier, learningset=learningset, k=param_dict['cv'])
    else:
        print('Running tdt_train on ' + args.input + ':')
        tree = tree_classifier.fit(learningset)

    tree.save(output=param_dict['output'])
    if 'accuracy' in tree.validation:
        print('Accuracy', tree.validation['accuracy'][0])
        print('Weighted accuracy', tree.validation['weighted-accuracy'][0])
    print('Done!')
