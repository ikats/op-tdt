import argparse
import os.path
import decisiontree.core.LearningSetBuilder as Lsb
import decisiontree.core.DecisionTree as Dt
my_path = os.path.abspath(os.path.dirname(__file__))

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Welcome to TDT testing builder')
    parser.add_argument('-model', type=str, help='learningset path')
    parser.add_argument('-testset', type=str, help='testset path')
    parser.add_argument('-output', type=str, help='output path directory')
    parser.add_argument('-config', type=str, help='path to config file')

    args = parser.parse_args()

    if not args.model:
        raise IOError('No input learningset was given')
    if not args.testset:
        raise IOError('No testset was given')

    if args.config:
        config_path = args.config
    else:
        config_path = my_path + '/' + 'TDT_builder.config'

    param_dict = dict()
    try:
        with open(config_path, 'r') as config_file:
            for line in config_file.readlines():
                split_line = line.split('=')
                split_line[0] = split_line[0].strip()
                if len(split_line) < 2:
                    param_dict[split_line[0]] = None
                else:
                    param_dict[split_line[0]] = split_line[1]
    except IOError as error:
        raise IOError('File ' + config_path + '  was not found')

    if param_dict['output'] is None:
        param_dict['output'] = my_path + '/tree.json'

    if args.output:
        param_dict['output'] = args.output

    directory_path = param_dict['output'].split('/')
    directory_path = '/'.join(directory_path[0: len(directory_path) - 1])
    if directory_path and not os.path.exists(directory_path):
        raise IOError('Directory ' + directory_path + ' does not exist!')

    ls_builder = Lsb.LearningSetBuilder()

    tree = None
    print('Running tdt_test on ' + args.testset + ':')
    try:
        tree = Dt.DecisionTree()
        tree.copy(args.model)
    except IOError as error:
        raise IOError('File ' + args.model + ' does not exist!')

    try:
        testset = ls_builder.build(ls_format='one_line',
                                   input_file=args.testset,
                                   ls_name='testset', obs_prefix='obs_test_', ts_prefix='TS_test_')
    except IOError as error:
        raise IOError('File ' + args.testset + ' does not exist!')

    try:
        tree.predict(testset)
    except RuntimeError as error:
        raise RuntimeError('Error occurred while running tdt_test.py!')

    tree.save(output=param_dict['output'])
    print('Accuracy', tree.validation['accuracy'][0])
    print('Done!')
