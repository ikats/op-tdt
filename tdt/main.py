import json
import os

FILENAME = os.path.join(os.path.dirname(__file__),'output_for_TDT.json')

def tdt_run(*args):
    with open(FILENAME) as file:
        data = json.load(file)
        print(data)
        return data
