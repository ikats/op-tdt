"""
PruningObject holds the information needed to prune a DecisionTree
"""


class PruningObject(object):

    def __init__(self, parent_id=None, children_list=None, depth=None):

        self._parent_id = parent_id
        self._children_list = children_list
        self._depth = depth

    @property
    def parent_id(self):
        return self._parent_id

    @parent_id.setter
    def parent_id(self, value):
        self._parent_id = value

    @property
    def children_list(self):
        return self._children_list

    @children_list.setter
    def children_list(self, value):
        self._children_list = value

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        self._depth = value

    def __cmp__(self, other):

        """
        Compare two PruningObject objects
        :param other: PruningObject instance
        :return: 0 if equal, 1 if self is deeper, -1 if self is shallower
        """

        if self._parent_id == other.parent_id:
            return 0

        if self._depth > other.depth:
            return 1

        if self._depth == other.depth:
            if self._children_list[0] > other.children_list[0]:
                return 1

        return -1

    def __eq__(self, other):

        return self._parent_id == other.parent_id

    def __lt__(self, other):

        if self._depth < other.depth:
            return True

        if self._depth == other.depth:
            if self._children_list[0] < other.children_list[0]:
                return True

        return False
