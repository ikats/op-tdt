
class WeightMatrix(object):

    def __init__(self,
                 var_id=None,
                 distance_measure=None,
                 weight_type=None,
                 weight_factor=None,
                 values=None,
                 paths=None):

        self._var_id = var_id
        self._distance_measure = distance_measure
        self._weight_type = weight_type
        self._weight_factor = weight_factor
        self._values = values
        self._paths = paths

    @property
    def var_id(self):
        return self._var_id

    @var_id.setter
    def var_id(self, value):
        self._var_id = value

    @property
    def distance_measure(self):
        return self._distance_measure

    @distance_measure.setter
    def distance_measure(self, value):
        self._distance_measure = value

    @property
    def weight_type(self):
        return self._weight_type

    @weight_type.setter
    def weight_type(self, value):
        self._weight_type = value

    @property
    def weight_factor(self):
        return self._weight_factor

    @weight_factor.setter
    def weight_factor(self, value):
        self._weight_factor = value

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, value):
        self._values = value

    @property
    def paths(self):
        return self._paths

    @paths.setter
    def paths(self, value):
        self._paths = value
