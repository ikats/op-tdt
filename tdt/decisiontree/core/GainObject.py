
"""
The class GainObject will be used to hold information about the observations
when we split a node.
"""


class GainObject(object):

    def __init__(self, obs_index=None, label=None, distance=None, partition=False):

        """

        :param obs_index: observation unique id
        :param label: observation class label
        :param distance: distance value or variable value (timeseries or standard variable)
        :param partition: used to indicate if it belong to the left sub-node or right sub-node in local search
        """

        self._obs_index = obs_index
        self._label = label
        self._distance = distance
        self._partition = partition

    @property
    def obs_index(self):
        return self._obs_index

    @obs_index.setter
    def obs_index(self, value):
        self._obs_index = value

    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, value):
        self._label = value

    @property
    def distance(self):
        return self._distance

    @distance.setter
    def distance(self, value):
        self._distance = value

    @property
    def partition(self):
        return self._partition

    @partition.setter
    def partition(self, value):
        self._partition = value

    def __cmp__(self, other):

        """
        Compare two GainObject objects
        :param other: Another GainObject to compare to
        :return: 0 if they hold the same obs_id, 1 if self.value > other.value, -1 otherwise
        """

        if self._distance == other.distance:
            return 0

        if self._distance < other.distance:
            return -1

        return 1

    def __eq__(self, other):

        return self._distance == other.distance

    def __lt__(self, other):

        return self._distance < other.distance
