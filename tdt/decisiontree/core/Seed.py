
class Seed(object):

    def __init__(self,
                 key=None,
                 lower_bounds=None,
                 upper_bounds=None,
                 gain=None,
                 depth=0,
                 label_stats=None,
                 split_keys=None,
                 split_value=0,
                 margin=0,
                 parent_seed=None,
                 n_neighbours=None,
                 is_hyper=False,
                 update_value=0):

        self._key = key
        self._lower_bounds = lower_bounds
        self._upper_bounds = upper_bounds

        # Split gain
        if gain is None:
            self._gain = (1, 0, 0)
        else:
            self._gain = gain

        self._depth = depth
        self._label_stats = label_stats
        self._split_keys = split_keys
        self._split_value = split_value
        self._margin = margin
        self._parent_seed = parent_seed
        self._n_neighbours = n_neighbours
        self._is_hyper = is_hyper
        self._update_value = update_value

    @property
    def key(self):
        return self._key

    @key.setter
    def key(self, value):
        self._key = value

    @property
    def lower_bounds(self):
        return self._lower_bounds

    @lower_bounds.setter
    def lower_bounds(self, value):
        self._lower_bounds = value

    @property
    def upper_bounds(self):
        return self._upper_bounds

    @upper_bounds.setter
    def upper_bounds(self, value):
        self._upper_bounds = value

    @property
    def gain(self):
        return self._gain

    @gain.setter
    def gain(self, value):
        self._gain = value

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        self._depth = value

    @property
    def label_stats(self):
        return self._label_stats

    @label_stats.setter
    def label_stats(self, value):
        self._label_stats = value

    @property
    def split_keys(self):
        return self._split_keys

    @split_keys.setter
    def split_keys(self, value):
        self._split_keys = value

    @property
    def split_value(self):
        return self._split_value

    @split_value.setter
    def split_value(self, value):
        self._split_value = value

    @property
    def margin(self):
        return self._margin

    @margin.setter
    def margin(self, value):
        self._margin = value

    @property
    def parent_seed(self):
        return self._parent_seed

    @parent_seed.setter
    def parent_seed(self, value):
        self._parent_seed = value

    @property
    def n_neighbours(self):
        return self._n_neighbours

    @n_neighbours.setter
    def n_neighbours(self, value):
        self._n_neighbours = value

    @property
    def is_hyper(self):
        return self._is_hyper

    @is_hyper.setter
    def is_hyper(self, value):
        self._is_hyper = value

    @property
    def update_value(self):
        return self._update_value

    @update_value.setter
    def update_value(self, value):
        self._update_value = value

    def __cmp__(self, other):

        """
        Compare two GainObject objects
        :param other: Another GainObject to compare to
        :return: 0 if they hold the same obs_id, 1 if self.value > other.value, -1 otherwise
        """

        if self._gain[0] == other.gain[0]:
            return 0

        if self._gain[0] < other.gain[0]:
            return 1

        return -1

    def __eq__(self, other):

        return self._gain[0] == other.gain[0]

    def __lt__(self, other):

        return self._gain[0] < other.gain[0]

    def __str__(self):
        return str(self._key)
