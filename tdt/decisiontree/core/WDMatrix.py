
class WDMatrix(object):

    def __init__(self,
                 method=None,
                 weight_type=None,
                 weight_factor=None,
                 values=None,
                 weights=None):

        self._method = method
        self._weight_type = weight_type
        self._weight_factor = weight_factor

        self._values = values
        self._weights = weights

    @property
    def method(self):
        return self._method

    @method.setter
    def method(self, value):
        self._method = value

    @property
    def weight_type(self):
        return self._weight_type

    @weight_type.setter
    def weight_type(self, value):
        self._weight_type = value

    @property
    def weight_factor(self):
        return self._weight_factor

    @weight_factor.setter
    def weight_factor(self, value):
        self._weight_factor = value

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, value):
        self._values = value

    @property
    def weights(self):
        return self._weights

    @weights.setter
    def weights(self, value):
        self._weights = value
