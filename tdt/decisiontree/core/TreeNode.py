from collections import OrderedDict


class TreeNode(object):

    """
    The TreeNode stores a node in the decision tree and all its stats.
    """

    def __init__(self,
                 name=None,
                 data=None,
                 node_variable=None,
                 children=None,
                 set_ids=None,
                 gain=None,
                 label_stats=None,
                 node_type=None):

        """
        :param data: a dictionary of key-val pairs, it will store
        the criteria of the split of the children and its related data.
        :param children: an array of TreeNode
        :param set_ids: a set of obs_id (check LearningSet)
        :param gain: the gain value of the node
        :param label_stats: a Counter for the labels of set_ids
        :param node_type: node type as an ouput
        """

        self._name = name

        # To understand the use of this attribute, check each export function
        if data is None:
            self._data = dict()
        else:
            self._data = data

        self._gain = gain

        # Type of the node as a Variable (input)
        self._node_variable = node_variable
        # Type pf the node as an output
        self._node_type = node_type

        self._children = children
        self._label_stats = label_stats
        self._set_ids = set_ids

        self._exporter = None
        self._type_exporter = {'pattern': self.export_pattern, 'variable': self.export_variable,
                               'similarity': self.export_similarity, 'comparison': self.export_comparison}

    # Getters and Setters
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def gain(self):
        return self._gain

    @gain.setter
    def gain(self, value):
        self._gain = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @property
    def node_variable(self):
        return self._node_variable

    @node_variable.setter
    def node_variable(self, value):
        self._node_variable = value

    @property
    def node_type(self):
        return self._node_type

    @node_type.setter
    def node_type(self, value):
        self._node_type = value
        if value != 'leaf':
            self._exporter = self._type_exporter[value]

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, value):
        self._children = value

    @property
    def label_stats(self):
        return self._label_stats

    @label_stats.setter
    def label_stats(self, value):
        self._label_stats = value

    @property
    def set_ids(self):
        return self._set_ids

    @set_ids.setter
    def set_ids(self, value):
        self._set_ids = value

    # Getter and Setter for the exporter (each node has a different exporter)
    def get_exporter(self, node_id):
        return self._exporter(node_id)

    def export_pattern(self, node_id):

        """
        Export a pattern node.

        :param node_id: unique Id for the node
        :return: a description of the node split criteria
        """

        description = OrderedDict()

        # Name of the pattern (trivial for now)
        description['variable'] = self.node_variable.name

        # Type of the pattern (float, string, ...)
        description['type'] = self.node_variable.type

        # Unit of the pattern (km, miles, ...)
        description['unit'] = self.node_variable.unit

        # Pattern description
        p_description = self.node_variable.description
        description['pattern'] = p_description

        # Criteria of the split
        criteria = []
        for i in range(len(self.data['threshold'])):
            node_id += 1

            # Test condition of the first child, value to compare, child node_id
            criteria.append({'test': self.data['threshold'][i],
                             'value': self.data['values'][i],
                             'child': node_id})

        description['criteria'] = criteria
        return description

    def export_variable(self, node_id):

        """
        Export a variable node.

        :param node_id: unique Id for the node
        :return: a description of the node split criteria
        """

        description = OrderedDict()

        # Name of the variable
        description['variable'] = self.node_variable.name

        # Type of the variable (float, string, ...)
        description['type'] = self.node_variable.type

        # Unit of the variable (km, miles, ...)
        description['unit'] = self.node_variable.unit

        # Criteria of the split
        criteria = []
        for i in range(len(self.data['threshold'])):
            node_id += 1

            # Test condition of the first child, value to compare, child node_id
            criteria.append({'test': self.data['threshold'][i],
                             'value': self.data['values'][i],
                             'child': node_id})

        description['criteria'] = criteria
        return description

    def export_similarity(self, node_id):

        """
        Export a similarity node (split a node using one timeseries)
        Information needed:
        variable name
        variable type
        sequence: obs_id (to get the timeseries)
        distance: information about the used distance measure (type and parameters)
        Criteria: split conditions, values and children node_ids

        :param node_id: unique Id for the node
        :return: a description of the node split criteria:
        """

        description = OrderedDict()
        description['variable'] = self.node_variable.name
        description['type'] = self.node_variable.type
        description['sequence'] = self.data['sequences'][0]['sequence']
        description['distance'] = self.data['distance']
        description['weight'] = self.data['weight']
        description['average_distance'] = self.data['average_distance']
        description['raw_weight_sum'] = self.data['raw_weight_sum']

        criteria = []
        for val in self.data['threshold']:
            node_id += 1
            criteria.append({'test': val,
                             'value': self.data['values'][0],
                             'child': node_id})
        description['criteria'] = criteria

        return description

    def export_comparison(self, node_id):

        """
        Export a comparison node (split a node using +2 timeseries)
        Information needed:
        variable name
        variable type
        distance: information about the used distance measure (type and parameters)
        Criteria: children sequences (obs_id to fetch the related timeseries) and their nodes ids

        :param node_id: unique Id for the node
        :return: a description of the node split criteria
        """

        description = OrderedDict()
        description['variable'] = self.node_variable.name
        description['type'] = self.node_variable.type
        description['distance'] = self.data['distance']
        description['weight'] = self.data['weight']
        description['average_distance'] = self.data['average_distance']
        description['raw_weight_sum'] = self.data['raw_weight_sum']
        criteria = []
        for val in self.data['sequences']:
            node_id += 1
            criteria.append({'sequence': val['sequence'],
                             'child': node_id})
        description['criteria'] = criteria

        return description
