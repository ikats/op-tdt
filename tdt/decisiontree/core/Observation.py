
class Observation(object):
    """
    This class represent a row in the LearningSet,
    every Observation should have an id and a label(None in case of unsupervised learning),
    and finally an np array of values/timeseries.
    """

    def __init__(self, obs_id=None, data=None, label=None):
        """

        :param obs_id: unique id for the observation
        :param data: the real values, [V1, V2, ..., TS1, TS2, ...]
        :type data: np array of values/arrays of dtype=object
        :param label: label of the observation
        :type label: string
        """
        self._obs_id = obs_id
        self._data = data
        self._label = label

    # Getters and Setters
    @property
    def obs_id(self):
        return self._obs_id

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, value):
        self._label = value

    def __getitem__(self, idx):
        return self._data[idx]
