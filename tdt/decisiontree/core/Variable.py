import tdt.decisiontree.core.Type as Tp


class Variable(object):
    """
    A description of a variable in our LearningSet,
    a variable has a name, a value type (string, int, ...),
    implementation type (continuous, nominal, timeseries, ...),
    a range (min, max) or (list of possible values),
    a unit, e.g. (km, miles).
    and finally a description.

    An example
    variable = Variable(name='speed', v_type='float', imp_type=Tp.Type.CONTINUOUS, v_range=(0, max), unit='km')

    """

    def __init__(self, name=None,
                 v_type=None,
                 imp_type=Tp.Type.CONTINUOUS,
                 v_range=None,
                 unit=None,
                 description=None):

        """
        :param name: optional name of the variable (unique)
        :param v_type: value type of the values of the variable (string, float, ...)
        :param imp_type: continuous, nominal, ... (Object of Type)
        :param v_range: range of the possible values
        :param unit: unit of the values (km, m2, ...)
        :param description: any other meaningful information in a key-value format (dict)
        """

        self._name = name
        self._v_type = v_type
        assert isinstance(imp_type, Tp.Type), "Type of %s is unknown" % name
        self._imp_type = imp_type
        self._range = v_range
        self._unit = unit
        self._description = description

    # Getters and Setters
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def type(self):
        return self._v_type

    @type.setter
    def type(self, value):
        self._v_type = value

    @property
    def imp_type(self):
        return self._imp_type

    @imp_type.setter
    def imp_type(self, value):
        self._imp_type = value

    @property
    def range(self):
        return self._range

    @range.setter
    def range(self, value):
        self._range = value

    @property
    def unit(self):
        return self._unit

    @unit.setter
    def unit(self, value):
        self._unit = value

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value
