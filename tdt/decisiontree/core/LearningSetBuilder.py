import csv
import os
from collections import OrderedDict

import numpy as np
import tdt.decisiontree.core.Utility as Ut
import tdt.decisiontree.core.LearningSet as Ls
import tdt.decisiontree.core.Observation as Obs
import tdt.decisiontree.core.Timeseries as Ts
import tdt.decisiontree.core.Type as Tp
import tdt.decisiontree.core.Variable as Vrb


class LearningSetBuilder(object):

    """
    A builder for the learningset, this class will handle different format of dataset
    (csv, one line per TS, one file per TS, ...)
    """

    def __init__(self):

        self._build_learningset = {'one_line': self.build_from_line, 'one_line_d': self.build_from_line_wd,
                                   'line_csv': self.build_from_line_csv, 'csv': self.build_from_csv}

    def build(self, ls_format,
              input_file=None,
              delimiter=',',
              ls_name='learningset',
              ts_prefix='TS_',
              obs_prefix='obs_',
              csv_path=None):

        return self._build_learningset[ls_format](input_file, delimiter, ls_name, ts_prefix, obs_prefix, csv_path)

    def build_from_line(self, input_file, delimiter, ls_name, ts_prefix, obs_prefix, csv_path):

        if not os.path.exists(input_file):
            raise IOError("No such file or directory ({})".format(input_file))

        variables = [Vrb.Variable('value', 'float', Tp.Type.TIMESERIES, (-5, 5))]

        observations = []
        with open(input_file, 'r') as _file:
            lines = _file.readlines()
            line_id = 0
            nb_digit = len(str(len(lines)))
            for line in lines:
                _arr = line.split(delimiter)
                data = []
                if len(_arr) > 1:
                    label = str(_arr[0])
                    obs_id = obs_prefix + str(line_id).zfill(nb_digit)
                    ts_name = ts_prefix + str(line_id).zfill(nb_digit) + '_V'
                    ts_object = Ts.Timeseries(name=ts_name, data=np.array([x for x in _arr[1:]], dtype=np.float64))
                    data.append(ts_object)
                    observation = Obs.Observation(obs_id=obs_id, data=data, label=label)
                    observations.append(observation)
                line_id += 1

        return Ls.LearningSet(name=ls_name,
                              observations=np.array(observations, dtype=object),
                              variables=np.array(variables, dtype=object))

    def build_from_line_wd(self, input_file, delimiter, ls_name, ts_prefix, obs_prefix, csv_path):

        if not os.path.exists(input_file):
            raise IOError("No such file or directory ({})".format(input_file))

        variables = [Vrb.Variable('value', 'float', Tp.Type.TIMESERIES, (-5, 5)),
                     Vrb.Variable('derivative', 'float', Tp.Type.TIMESERIES, (-5, 5))]

        observations = []
        with open(input_file, 'r') as _file:
            lines = _file.readlines()
            line_id = 0
            nb_digit = len(str(len(lines)))
            for line in lines:
                _arr = line.split(delimiter)
                data = []
                if len(_arr) > 1:
                    label = str(_arr[0])
                    obs_id = obs_prefix + str(line_id).zfill(nb_digit)
                    ts_name = ts_prefix + str(line_id).zfill(nb_digit)
                    ts_values = np.array(_arr[1:], dtype=np.float64)
                    ts_object = Ts.Timeseries(name=ts_name + '_v', data=ts_values)
                    ts_ds_data = np.ediff1d(ts_values)
                    ts_dr_object = Ts.Timeseries(name=ts_name + '_dr', data=ts_ds_data)
                    data.append(ts_object)
                    data.append(ts_dr_object)
                    observation = Obs.Observation(obs_id=obs_id, data=data, label=label)
                    observations.append(observation)
                line_id += 1

        return Ls.LearningSet(name=ls_name,
                              observations=np.array(observations, dtype=object),
                              variables=np.array(variables, dtype=object))

    def build_from_csv(self, input_file, delimiter, ls_name, ts_prefix, obs_prefix, csv_path):

        if not os.path.exists(csv_path):
            raise IOError("No such file or directory ({})".format(csv_path))
        variables = []

        with open(csv_path, 'r') as file:
            reader = csv.reader(file)
            csv_data = list(reader)

        """
        for ind in range(len(csv_data)):
            csv_data[ind] = csv_data[ind][0].split(delimiter)
        """

        pat_index = 0
        cur_pat = csv_data[0][0]
        nb_labels = 0
        for var in csv_data[0][0:len(csv_data[0]) - 1]:
            var_clean = var.replace('C#', '')
            if 'nb_' in var_clean:
                nb_labels += 1
                continue
            else:
                pat_desc = self.get_patten_description(var_clean)
                if pat_desc['definition'] != cur_pat:
                    pat_index += 1
                    cur_pat = pat_desc['definition']
                pat_name = 'Pat' + str(pat_index) + '_' + pat_desc['operator']

                variables.append(Vrb.Variable(name=pat_name,
                                              v_type='float',
                                              imp_type=Tp.Type.PATTERN,
                                              description=pat_desc))

        csv_data = csv_data[1:]
        observations = []
        nb_digit = len(str(len(csv_data)))
        for ind in range(len(csv_data)):
            data = []
            label = str(csv_data[ind][-1])
            obs_id = obs_prefix + str(ind).zfill(nb_digit)
            del csv_data[ind][-1]
            csv_data[ind] = csv_data[ind][nb_labels:]
            csv_data[ind] = list(map(float, csv_data[ind]))
            data.extend(csv_data[ind])
            observation = Obs.Observation(obs_id=obs_id, data=np.array(data, dtype=object), label=label)
            observations.append(observation)

        return Ls.LearningSet(name=ls_name,
                              observations=np.array(observations, dtype=object),
                              variables=np.array(variables, dtype=object))

    def build_from_csv_2(self, input_file, delimiter, ls_name, ts_prefix, obs_prefix, csv_path):

        if not os.path.exists(csv_path):
            raise IOError("No such file or directory ({})".format(csv_path))
        variables = []

        with open(csv_path, 'r') as file:
            reader = csv.reader(file)
            csv_data = list(reader)

        for ind in range(len(csv_data)):
            csv_data[ind] = csv_data[ind][0].split(';')

        nb_labels = 0
        for var in csv_data[0][0:len(csv_data[0]) - 1]:
            var_clean = var.replace('C#', '')
            pat_desc = self.get_patten_description(var_clean)
            pat_description = pat_desc
            pat_name = var_clean

            pat_description['variable'] = 'pattern name'
            variables.append(Vrb.Variable(name=pat_name,
                                          v_type='float',
                                          imp_type=Tp.Type.PATTERN,
                                          description=pat_description))

        csv_data = csv_data[1:]
        observations = []
        nb_digit = len(str(len(csv_data)))
        for ind in range(len(csv_data)):
            data = []
            label = str(csv_data[ind][-1])
            obs_id = obs_prefix + str(ind).zfill(nb_digit)
            del csv_data[ind][-1]
            csv_data[ind] = csv_data[ind][nb_labels:]
            csv_data[ind] = list(map(float, csv_data[ind]))
            data.extend(csv_data[ind])
            observation = Obs.Observation(obs_id=obs_id, data=np.array(data, dtype=object), label=label)
            observations.append(observation)

        return Ls.LearningSet(name=ls_name,
                              observations=np.array(observations, dtype=object),
                              variables=np.array(variables, dtype=object))

    def build_from_line_csv(self, input_file, delimiter, ls_name, ts_prefix, obs_prefix, csv_path):

        if not os.path.exists(input_file):
            raise IOError("No such file or directory ({})".format(input_file))
        variables = []

        with open(csv_path, 'r') as file:
            reader = csv.reader(file)
            csv_data = list(reader)

        for ind in range(len(csv_data)):
            csv_data[ind] = csv_data[ind][0].split(';')

        pat_index = 0
        cur_pat = csv_data[0][0]
        nb_labels = 0
        # labels = dict()
        for var in csv_data[0][0:len(csv_data[0]) - 1]:
            var_clean = var.replace('C#', '')
            if 'nb_' in var_clean:
                nb_labels += 1
                """
                variables.append(Vrb.Variable(name=var_clean,
                                              v_type='float',
                                              imp_type=Tp.Type.CONTINUOUS))
                labels[nb_labels] = var_clean.split('patterns_')[1]
                nb_labels += 1
                """
                continue
            else:
                pat_desc = self.get_patten_description(var_clean)
                if pat_desc['definition'] != cur_pat:
                    pat_index += 1
                    cur_pat = pat_desc['definition']
                pat_name = 'Pat' + str(pat_index) + '_' + pat_desc['operator']

                variables.append(Vrb.Variable(name=pat_name,
                                              v_type='float',
                                              imp_type=Tp.Type.PATTERN,
                                              description=pat_desc))

        csv_data = csv_data[1:]

        variables.append(Vrb.Variable(ls_name, 'float', Tp.Type.TIMESERIES, (-5, 5)))

        observations = []
        with open(input_file, 'r') as _file:
            lines = _file.readlines()
            line_id = 0
            nb_digit = len(str(len(lines)))
            for ind in range(len(lines)):
                line = lines[ind]
                _arr = line.split(delimiter)
                data = []
                if len(_arr) > 1:
                    label = str(_arr[0])
                    obs_id = ls_name + '_' + obs_prefix + str(line_id).zfill(nb_digit)
                    ts_name = ls_name + '_' + ts_prefix + str(line_id).zfill(nb_digit) + '_V'
                    del csv_data[ind][-1]

                    #########################################
                    # Important for non-mv-files
                    csv_data[ind] = csv_data[ind][nb_labels:]
                    #########################################
                    csv_data[ind] = list(map(float, csv_data[ind]))
                    data.extend(csv_data[ind])
                    ts_object = Ts.Timeseries(name=ts_name, data=np.array([x for x in _arr[1:]], dtype=np.float64))
                    data.append(ts_object)
                    observation = Obs.Observation(obs_id=obs_id, data=np.array(data, dtype=object), label=label)
                    observations.append(observation)
                line_id += 1

        return Ls.LearningSet(name=ls_name,
                              observations=np.array(observations, dtype=object),
                              variables=np.array(variables, dtype=object))

    def build_from_learningset(self, orig_learningset, keys):

        new_observations = orig_learningset.observations[keys]
        return Ls.LearningSet(name=orig_learningset.name, observations=new_observations,
                              variables=orig_learningset.variables, meta_data=orig_learningset.meta_data,
                              timestamp=orig_learningset.timestamp)

    def build_from_merging(self, original_set, new_set):
        new_observations = np.concatenate([original_set.observations, new_set.observations])
        return Ls.LearningSet(name=original_set.name, observations=new_observations, variables=original_set.variables,
                              meta_data=original_set.meta_data, timestamp=original_set.timestamp)

    def get_patten_description(self, name):

        p_description = OrderedDict()
        p_description['generator'] = ['HARP', ['version', 1.0]]
        p_info = name.split('/')
        operator = p_info[1]
        p_temp = p_info[0].split('(')
        p_def = p_temp[0]
        p_vars = p_temp[1].split(')')[0]

        breakpoints = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        domain = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']
        # for now, need to be modified
        p_vars = [{'name': v, 'breakpoints': breakpoints, 'domain': domain} for v in p_vars.split('-')]

        reg_len = Ut.pattern_to_re(p_def)
        p_description['definition'] = reg_len[0]
        p_description['slice'] = [0, 100]  # for now
        p_description['operator'] = operator
        p_description['variables'] = p_vars

        return p_description
